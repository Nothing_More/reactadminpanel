import {combineReducers} from 'redux';
import usersReducer from './usersReducer';
import authReducer from './authReducer';
import adminReducers from './adminReducers';

export default combineReducers({
    users: usersReducer,
    auth: authReducer,
    admins: adminReducers
});